FROM registry.gitlab.com/programming-knights/laravel-base

COPY ./local/app/ /var/www/html/
WORKDIR /var/www/html
RUN chgrp www-data -R storage bootstrap/cache public
RUN chmod -R ug+rwx storage bootstrap/cache public
