FROM alpine:3 as download
RUN wget -O composer-installer https://getcomposer.org/installer
RUN wget -O unzip.tar https://oss.oracle.com/el4/unzip/unzip.tar && \
    tar xvf unzip.tar

FROM ubuntu:20.04

# prevents apt install -yq from asking post-install interactive questions
ENV DEBIAN_FRONTEND=noninteractive

# install unzip
COPY --from=download unzip /usr/local/bin/unzip

# install apache2, php, and composer
COPY --from=download composer-installer /tmp/composer-installer
RUN apt update && \
    apt install -yq apache2 && \
    apt install -yq php libapache2-mod-php php-mbstring php-xmlrpc php-soap php-gd php-xml php-cli php-zip php-bcmath php-tokenizer php-json php-pear php-mysql php-pgsql && \
    cat /tmp/composer-installer | php && \
    mv composer.phar /usr/local/bin/composer && \
    chmod +x /usr/local/bin/composer

# redirect Apache logs to stdout and stderr
RUN rm /var/log/apache2/access.log /var/log/apache2/error.log && \
    ln -s /dev/stdout /var/log/apache2/access.log && \
    ln -s /dev/stderr /var/log/apache2/error.log

# configure Apache
RUN \
    # enable the rewrite mod
    ln -s /etc/apache2/mods-available/rewrite.load /etc/apache2/mods-enabled/ && \
    \
    # set the `AllowOverride` directive to `All` in apache2.conf for /var/www
    perl -i.original -p0e 's,<Directory /var/www/>\s+Options Indexes FollowSymLinks\s+AllowOverride None,<Directory /var/www/>\n        Options Indexes FollowSymLinks\n        AllowOverride All,g' /etc/apache2/apache2.conf && \
    \
    # change the document root to /var/www/html/public
    sed -ie 's,DocumentRoot /var/www/html,DocumentRoot /var/www/html/public,g' /etc/apache2/sites-available/000-default.conf

EXPOSE 80

CMD apache2ctl -D FOREGROUND
